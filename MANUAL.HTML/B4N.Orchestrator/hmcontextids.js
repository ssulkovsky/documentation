﻿var hmContextIds = new Array();
function hmGetContextId(query) {
    var urlParams;
    var match,
        pl = /\+/g,
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
    params = {};
    while (match = search.exec(query))
       params[decode(match[1])] = decode(match[2]);
    if (params["contextid"]) return decodeURIComponent(hmContextIds[params["contextid"]]);
    else return "";
}

hmContextIds["2"]="terminy_i_opredeleniya.htm";
hmContextIds["1"]="obratnaya_svyaz`.htm";
hmContextIds["3"]="pol`zovatel`skij_interfejs.htm";
hmContextIds["4"]="avtorizatsiya_na_portale.htm";
hmContextIds["6"]="osnovnye_operatsii.htm";
hmContextIds["5"]="opisanie_portala.htm";
hmContextIds["21"]="panel`_instrumentov.htm";
hmContextIds["24"]="uchyotnaya_zapis`.htm";
hmContextIds["25"]="administrirovanie.htm";
hmContextIds["26"]="yazyk_interfejsa.htm";
hmContextIds["22"]="panel`_navigatsii.htm";
hmContextIds["23"]="rabochaya_oblast`.htm";
hmContextIds["8"]="osnovnye_kategorii_portala.htm";
hmContextIds["10"]="kontrollery.htm";
hmContextIds["11"]="klastery.htm";
hmContextIds["12"]="konfigurirovanie_klasterov.htm";
hmContextIds["13"]="kommutatory.htm";
hmContextIds["14"]="topologiya.htm";
hmContextIds["19"]="igmp_istochniki.htm";
hmContextIds["20"]="nastrojki_logirovaniya.htm";
